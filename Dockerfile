FROM golang:1.16-alpine AS build
ARG SERVICE
RUN /bin/sh -c 'echo -e $SERVICE'
RUN apk update && apk add make git gcc musl-dev
ADD . /go/src/TWFyY2luIEdvZ29BcHBzIE5BU0EK
WORKDIR /go/src/TWFyY2luIEdvZ29BcHBzIE5BU0EK

RUN make install
RUN make $SERVICE
RUN mv $SERVICE /$SERVICE

FROM alpine:3.11
ARG SERVICE
ARG ADDR
ARG DB
ENV SERVICE=$SERVICE
RUN apk add --no-cache ca-certificates && mkdir /app
COPY --from=build /$SERVICE /app/$SERVICE

ENTRYPOINT /app/$SERVICE