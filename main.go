package main

import (
	"log"
	"os"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/utils/semaphore"

	"github.com/urfave/cli/v2"
	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/api/nasa"
	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/service"
)

const (
	appName    = "url-collector"
	appDesc    = "Allows fetching list of urls."
	appVersion = "0.1.0"
	baseURL    = "https://api.nasa.gov/planetary/apod"
)

var flags = []cli.Flag{
	&cli.StringFlag{
		Name:  "API_KEY",
		Usage: "api key used for NASA API requests",
		Value: "DEMO_KEY",
	},
	&cli.IntFlag{
		Name:  "CONCURRENT_REQUESTS",
		Usage: "api limit used for NASA API requests",
		Value: 5,
	},
	&cli.IntFlag{
		Name:  "PORT",
		Usage: "a port the api will be served at",
		Value: 8080,
	},
}

func main() {
	app := cli.NewApp()
	app.Flags = flags
	app.Name = appName
	app.Description = appDesc
	app.Version = appVersion
	app.Action = func(cliCtx *cli.Context) error {
		maxConcurrentRequests := cliCtx.Int("CONCURRENT_REQUESTS")
		sem := semaphore.New(maxConcurrentRequests)
		nasaApi := nasa.New(baseURL, cliCtx.String("API_KEY"), sem)
		server := service.New(nasaApi, cliCtx.Int("PORT"))
		return server.Serve()
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalf("Failed to run service: %v", err)
	}
}
