module gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
)
