# ----- Installing -----

.PHONY: install
install:
	go mod download

# ----- Linting -----

.PHONY: lint
lint:
	golangci-lint run

# ----- Testing -----

BUILDENV := CGO_ENABLED=0
TESTFLAGS ?= -short -cover

.PHONY: test
test:
	$(BUILDENV) go test $(TESTFLAGS) ./...

# ----- Building -----

SERVICE ?= TWFyY2luIEdvZ29BcHBzIE5BU0EK

.PHONY: clean
clean:
	rm -f $(SERVICE)

# builds our binary
build:
	cd $(CURDIR); CGO_ENABLED=0 go build -o $(SERVICE)

$(SERVICE): build


# ----- Running -----
run:
	go run main.go

# ----- Docker -----

SERVICE_NAME = $(shell echo $(SERVICE) | tr A-Z a-z)
docker-image:
	docker build -f Dockerfile  --rm -t $(SERVICE_NAME):local . --build-arg SERVICE=$(SERVICE)
