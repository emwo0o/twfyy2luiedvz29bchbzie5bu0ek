# TWFyY2luIEdvZ29BcHBzIE5BU0EK

## url-collector API

The API is responsible for fetching list of URL's from 3rd party API (e.g. NASA APOD API).

### Available endpoints

    GET /pictures?start_date=2020-01-04&end_date=2020-02-05

### Expected responses

- Success
  

    {“urls”: ["https://apod.nasa.gov/apod/image/2008/AlienThrone_Zajac_3807.jpg", ...]}

- Failure


    {“error”: “error message”}

### Running locally

To run the project simply issue the command

    make run

### Running in docker

First create the image

    make docker-image

Run the container

    docker run -p 8080:8080 --rm -it twfyy2luiedvz29bchbzie5bu0ek:local

### Testing

To run unit tests all you have to do is to run

    make test

To make get tests coverage:

     go test -cover -count=1 -coverprofile=coverage.out ./...

To have coverage presented in more visual manner

    go tool cover -html=coverage.out

### Linting

To make sure the codebase error, stylistic, bug free run the linter. 

Make sure you have linter installed on your platform, by clicking on the [link](https://golangci-lint.run/usage/install/#local-installation)

Once installed

    make lint

## Current limitations/drawbacks

- If the range is too big server times out and fails. There are several ways this problem could be addressed.
    - One way could potentially be implementation of `recover` function (not tested) to make sure server is keeps running
    - Same additional validation rules could be applied to date range (e.g. not exceed 1 year etc)
    - parameterize timeouts on the http server
    - increase maxConcurrentConnections
    
- The code is not fully tested

## Discussion

- What if we were to change the NASA API to some other images provider?
  
  The NASA API implements `API` interface, all there is to do is to add new api in `api` package that satisfies the `API` interface, instantiate it in `main.go` and pass as an argument to `Server`
  
- What if, apart from using NASA API, we would want to have another microservice fetching urls from
European Space Agency. How much code could be reused?
  
  Apart from `nasa` package 99% of code could be reused.
  

- What if we wanted to add some more query params to narrow down lists of urls - for example,
selecting only images taken by certain person. (field copyright in the API response)
  
According to NASA APOD API documentation only fields listed below are supported as query parameters
        
        date
        start_date
        end_date
        count
        thumbs
        api_key

For this reason to add such filtering, we could add query parameter to our url as such

    GET /pictures?start_date=2020-01-04&end_date=2020-02-05&taken_by=john

And then perform filtering in the service to only get images taken by `john` discarding the rest.