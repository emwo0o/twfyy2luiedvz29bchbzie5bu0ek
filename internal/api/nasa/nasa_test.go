package nasa_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/utils/semaphore"

	"github.com/stretchr/testify/assert"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/api/nasa"
)

func Test_GetListOfUrls_ApiError(t *testing.T) {
	expected := nasa.CollectorApiError{
		Code:    http.StatusBadRequest,
		Message: "400 Bad Request",
	}
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, http.MethodGet, r.Method)
		assert.Equal(t, "/", r.URL.Path)
		w.WriteHeader(http.StatusBadRequest)
	}))
	defer server.Close()

	nasaApi := nasa.New(server.URL, "test-key", semaphore.New(2))
	resp := nasaApi.GetListOfUrls("2021-07-23", "2021-07-23")
	assert.Empty(t, resp.URLs)
	assert.Equal(t, expected, resp.Error)
}

func Test_GetListOfUrls_Success(t *testing.T) {
	expected := nasa.CollectorApiResponse{
		URLs: []string{"foo.com", "bar.com"},
	}

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := []nasa.ApiResponse{
			{URL: "foo.com"},
			{URL: "bar.com"},
		}
		assert.NoError(t, json.NewEncoder(w).Encode(&resp))
		assert.Equal(t, http.MethodGet, r.Method)
		assert.Equal(t, "/", r.URL.Path)
		w.WriteHeader(http.StatusOK)
	}))
	defer server.Close()

	nasaApi := nasa.New(server.URL, "test-key", semaphore.New(2))
	resp := nasaApi.GetListOfUrls("2021-07-23", "2021-07-23")
	assert.Empty(t, resp.Error)
	assert.Equal(t, expected, resp)
}
