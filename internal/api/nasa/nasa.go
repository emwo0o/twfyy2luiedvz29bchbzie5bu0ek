package nasa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"time"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/utils/semaphore"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/utils/timeutils"
)

type (
	// Api allows communication with Api
	Api struct {
		baseURL string
		apiKey  string
		sem     *semaphore.Semaphore
	}

	// ApiResponse represents response from Nasa Api
	ApiResponse struct {
		URL string `json:"url"`
	}

	// CollectorApiResponse represents response from the API
	CollectorApiResponse struct {
		URLs  []string          `json:"urls"`
		Error CollectorApiError `json:"error"`
	}

	// CollectorApiError customer error response
	CollectorApiError struct {
		Code    int
		Message string
	}
)

// Error implements error interface
func (apiErr CollectorApiError) Error() string {
	return fmt.Sprintf("error code: %d, error message: %s", apiErr.Code, apiErr.Message)
}

const dateLayout = "2006-01-02"

// New initializes connectivity with NASA Api
func New(baseURL, apiKey string, sem *semaphore.Semaphore) Api {
	return Api{
		apiKey:  apiKey,
		baseURL: baseURL,
		sem:     sem,
	}
}

// GetListOfUrls attempts to fetch list of images from Nasa Api
func (na Api) GetListOfUrls(startDate, endDate string) CollectorApiResponse {
	start, end, err := parseDates(startDate, endDate)
	if err != nil {
		return CollectorApiResponse{Error: *makeApiResponseError(http.StatusInternalServerError, err.Error())}
	}

	if err := validateDates(start, end); err != nil {
		return CollectorApiResponse{Error: *makeApiResponseError(http.StatusInternalServerError, err.Error())}
	}

	ranges := timeutils.SplitDateRange(start, end)
	count := int(math.Ceil(end.Sub(start).Hours()/24) + 2) // 2 is added as security measure so the call doesnt block

	urlsChan := make(chan string, count)
	errs := make(chan *CollectorApiError, len(ranges))

	for _, r := range ranges {
		na.sem.Add()
		go func(r timeutils.Range) {
			err := na.performRequest(r, urlsChan)
			if err != nil {
				errs <- err
			}
			na.sem.Done()
		}(r)
	}

	na.sem.Wait()
	close(errs)
	close(urlsChan)

	if len(errs) > 0 {
		if err := <-errs; err != nil {
			return CollectorApiResponse{Error: *err}
		}
	}

	urls := make([]string, 0)
	for u := range urlsChan {
		urls = append(urls, u)
	}

	return CollectorApiResponse{
		URLs: urls,
	}
}

func (na Api) performRequest(r timeutils.Range, urlsChan chan string) *CollectorApiError {
	req, err := http.NewRequest(http.MethodGet, na.baseURL, nil)
	if err != nil {
		return makeApiResponseError(http.StatusInternalServerError, err.Error())
	}

	sd := r.Start.Format(dateLayout)
	ed := r.End.Format(dateLayout)

	q := req.URL.Query()
	q.Add("api_key", na.apiKey)
	q.Add("start_date", sd)
	q.Add("end_date", ed)
	req.URL.RawQuery = q.Encode()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return makeApiResponseError(http.StatusInternalServerError, err.Error())
	}

	if resp.StatusCode != http.StatusOK {
		return makeApiResponseError(resp.StatusCode, resp.Status)
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return makeApiResponseError(http.StatusInternalServerError, err.Error())
	}

	data := make([]ApiResponse, 0)
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		return makeApiResponseError(http.StatusInternalServerError, err.Error())
	}

	for _, url := range data {
		urlsChan <- url.URL
	}

	return nil
}

// parseDates makes sure dates are formatted as expected by the API
// It is also first validation stage
func parseDates(startDate, endDate string) (time.Time, time.Time, error) {
	start, err := time.Parse(dateLayout, startDate)
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	end, err := time.Parse(dateLayout, endDate)
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	return start, end, nil
}

// validateDates applies additional validation rules to dates
func validateDates(startDate, endDate time.Time) error {
	// start date should be before end date
	if startDate.After(endDate) {
		return fmt.Errorf("end date (%s) must be after start date (%s)", endDate, startDate)
	}

	return nil
}

// makeApiResponseError helper function to create CollectorApiError
func makeApiResponseError(statusCode int, message string) *CollectorApiError {
	return &CollectorApiError{
		Code:    statusCode,
		Message: message,
	}
}
