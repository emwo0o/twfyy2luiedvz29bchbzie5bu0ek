package service

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/api/nasa"

	"github.com/gorilla/mux"
)

type (
	// Server represents http server that handles requests to url-collector service
	Server struct {
		api  API
		port int
	}

	// API is an interface to 3rd party service (e.g. NASA API)
	API interface {
		GetListOfUrls(startDate, endDate string) nasa.CollectorApiResponse
	}
)

// New instantiates new server
func New(api API, port int) Server {
	return Server{
		api:  api,
		port: port,
	}
}

// Serve runs the Collector Api Server
func (s Server) Serve() error {
	router := mux.NewRouter()
	router.HandleFunc("/pictures", s.GetListOfUrls()).Methods(http.MethodGet)
	wait := 15 * time.Second

	servingAddress := fmt.Sprintf(":%d", s.port)

	srv := &http.Server{
		Addr:         servingAddress,
		Handler:      router,
		ReadTimeout:  wait,
		WriteTimeout: wait,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	log.Printf("Serving at %s\n", servingAddress)

	// graceful shutdown via SIGINT (Ctrl+C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		return err
	}

	log.Println("shutting down")
	os.Exit(0)

	return nil
}

// GetListOfUrls queries underlying api for list of images
func (s Server) GetListOfUrls() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		//s.sem.Add()
		startDate := r.URL.Query().Get("start_date")
		endDate := r.URL.Query().Get("end_date")
		log.Printf("fetching urls for date_start %s and date_end %s", startDate, endDate)
		resp := s.api.GetListOfUrls(startDate, endDate)
		if resp.Error.Code != 0 || resp.Error.Message != "" {
			body := struct {
				Error string `json:"error"`
			}{
				Error: resp.Error.Message,
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(resp.Error.Code)
			err := json.NewEncoder(w).Encode(body)
			if err != nil {
				log.Fatal(err)
			}
			return
		}

		body := struct {
			URLs []string `json:"urls"`
		}{
			URLs: resp.URLs,
		}
		w.Header().Set("Content-Type", "application/json")
		err := json.NewEncoder(w).Encode(body)
		if err != nil {
			log.Fatal(err)
		}
	}
}
