package timeutils_test

import (
	"testing"
	"time"

	"gitlab.com/emwo0o/twfyy2luiedvz29bchbzie5bu0ek/internal/utils/timeutils"

	"github.com/stretchr/testify/assert"
)

func TestSplitDateRange(t *testing.T) {
	cases := []struct {
		name      string
		startDate time.Time
		endDate   time.Time
		expected  []timeutils.Range
	}{
		{
			name:      "same date",
			startDate: time.Date(2021, time.July, 20, 0, 0, 0, 0, time.UTC),
			endDate:   time.Date(2021, time.July, 20, 0, 0, 0, 0, time.UTC),
			expected: []timeutils.Range{
				{
					Start: time.Date(2021, time.July, 20, 0, 0, 0, 0, time.UTC),
					End:   time.Date(2021, time.July, 20, 0, 0, 0, 0, time.UTC),
				},
			},
		},
		{
			name:      "should return two ranges",
			startDate: time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC),
			endDate:   time.Date(2021, time.November, 12, 0, 0, 0, 0, time.UTC),
			expected: []timeutils.Range{
				{
					Start: time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC),
					End:   time.Date(2021, time.November, 2, 0, 0, 0, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.November, 3, 0, 0, 0, 0, time.UTC),
					End:   time.Date(2021, time.November, 12, 0, 0, 0, 0, time.UTC),
				},
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := timeutils.SplitDateRange(c.startDate, c.endDate)
			assert.Equal(t, c.expected, got)
		})
	}
}
