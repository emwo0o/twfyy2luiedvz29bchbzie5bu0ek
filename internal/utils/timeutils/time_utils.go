package timeutils

import (
	"math"
	"time"
)

// Range represents time range with start and end
type Range struct {
	Start time.Time
	End   time.Time
}

const maxBatchSize = 100

// SplitDateRange split date range into multiple batches
func SplitDateRange(start, end time.Time) (ranges []Range) {
	if start.Equal(end) {
		ranges = append(ranges, Range{
			Start: start,
			End:   end,
		})
		return
	}

	ra := Range{Start: start}

	days := math.Ceil(end.Sub(start).Hours() / 24)
	for i := 0; i <= int(days); i += maxBatchSize {
		ra.End = ra.Start.Add(maxBatchSize * 24 * time.Hour)
		if ra.End.After(end) {
			ra.End = end
			ranges = append(ranges, ra)
			break
		}
		ranges = append(ranges, ra)
		ra.Start = ra.End.Add(24 * time.Hour)
	}

	return
}
