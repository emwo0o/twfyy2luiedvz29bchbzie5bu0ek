package semaphore

import (
	"sync"
)

// Semaphore is buffered wait group
type Semaphore struct {
	c  chan struct{}
	wg *sync.WaitGroup
}

// New instantiates the Semaphore
func New(max int) *Semaphore {
	return &Semaphore{make(chan struct{}, max), new(sync.WaitGroup)}
}

// Add increments the WaitGroup counter by one.
func (s *Semaphore) Add() {
	s.wg.Add(1)
	s.c <- struct{}{}
}

// Done decrements the WaitGroup counter by one.
func (s *Semaphore) Done() {
	<-s.c
	s.wg.Done()
}

// Wait blocks until the WaitGroup counter is zero.
func (s *Semaphore) Wait() {
	s.wg.Wait()
}
